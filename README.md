# Moiré

Moiré will be a new DJ application written in Rust with a DAW-like timeline interface. Refer to the [roadmap](ROADMAP.md).

Come say hi on the [Zulip chat](https://moire.zulipchat.com/).

A moiré is ["an interference pattern produced by overlaying similar but slightly offset templates"](https://mathworld.wolfram.com/MoirePattern.html).

![♫ When the spacing is tight / And the difference is slight / That's a moiré ♫](assets/xkcd_color_pattern.png)

[XKCD comic](https://xkcd.com/1814/) by Randall Munroe licensed under [Creative Commons Attribution-NonCommercial 2.5 License](https://creativecommons.org/licenses/by-nc/2.5/).

Moiré is licensed under the [GNU General Public License version 3 or later](LICENSE.txt).

## Building & running

Moiré is written in Rust and has few C dependencies.

[JACK](https://jackaudio.org) is required on all operating systems. On Windows and macOS, libjack
does not need to be available at build time, only at runtime. On Linux, Pipewire JACK can be used
instead of JACK1 or JACK2 (`pipewire-jack-audio-connection-kit-devel` package on Fedora).
JACK/Pipewire needs to be running before running Moiré.

On Linux, at least one of the `x11` or `wayland` Cargo features must be enabled. Both are enabled
by default. The `x11` feature requires the xcb library (`libxcb-devel` package on Fedora).

On Linux, fontconfig is required. Also, an XDG Desktop Portal implementation that supports the
`org.freedesktop.portal.FileChooser` D-Bus API needs to be available at runtime. The KDE, GNOME,
and GTK XDG Desktop Portal backends provide this, but the wlroots backend does not. The wlroots
backend can be installed simultaneously with another backend that provides the `FileChooser` API.

After these dependencies are installed, building is as simple as:

```sh
cargo build
```

and running is as simple as:

```sh
cargo run
```

## Development

Various development tasks are automated using [_just recipes_](https://github.com/casey/just)
that are configured in [`.justfile`](./.justfile).

Use _Cargo_ for installing the `just` command:

```sh
cargo install just
```

Get started by installing (or updating) the dev tools required for the project:

```sh
just setup
```

List the available _recipes_:

```sh
just
```

Upgrade all dependencies periodically:

```sh
just upgrade
```

Check that all crates can be built independently after modifying dependencies:

```sh
just check
```

Each commit should pass the _pre-commit_ hook checks:

```sh
just pre-commit
```

## Goals

- Equally usable as a casual music player application on a phone in a pocket or a full fleged DJ software on a laptop
- Support features common on commercially available contemporary DJ controllers from popular manufacturers
- Eventually support timecode vinyl control
- Eventually crossplatform (Linux, Windows, potentially Android) but focusing on Linux first (desktop and mobile Linux)
- Strive for designs that work for a wide variety of use cases before resorting to adding options
- Actively work to solve problems upstream and collaborate with other projects
- Purely FOSS without proprietary dependencies beyond operating system APIs
- Use FOSS infrastructure as much as practical (exceptions can be made, for example, for building the application on proprietary operating systems)
- Consensus-based decision making and cooperative, nonhierarchical governance. Irreconcilable disagreements will be decided promptly by voting. Details will be worked out as more people join.

## Non-goals

- Video playback, mixing, manipulation, or generation. Communication with external video applications over standard protocols would be welcome though.
- Imitating other DJ software verbatim. This is its own application. Proposals will be evaluated on their own merits, not simply because another software does it that way.
- Antifeatures (functionality that acts against the interest of the user), for example DRM. Antifeatures also include interacting with any network services such as music streaming services whose terms of service would require implementing artificial restrictions.
