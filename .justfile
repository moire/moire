# just manual: https://github.com/casey/just/#readme

_default:
    @just --list

# Set up (and update) tooling
setup:
    # Ignore rustup failures, because not everyone might use it
    rustup self update || true
    # cargo-edit is needed for `cargo upgrade`
    cargo install cargo-edit just
    pip install -U pre-commit
    #pre-commit install --hook-type pre-commit

# Upgrade (and update) dependencies
upgrade: setup
    pre-commit autoupdate
    # Aggressively try to upgrade all dependencies, revert unwanted upgrades manually as needed.
    cargo upgrade --incompatible --pinned
    cargo update

# Format source code
fmt:
    cargo fmt --all

# Run pre-commit hooks
pre-commit:
    SKIP=no-commit-to-branch pre-commit run --all-files

# Check all crates individually (takes a long time)
check:
    cargo check --locked --all-targets -p moire
    cargo check --locked --all-targets -p moire-app
    cargo check --locked --all-targets -p moire-audio-dsp
    cargo check --locked --all-targets -p moire-audio-io
    cargo check --locked --all-targets -p moire-ctrl
    cargo check --locked --all-targets -p moire-hid-io
    cargo check --locked --all-targets -p moire-library
    cargo check --locked --all-targets -p moire-ui
    cargo check --locked --all-targets -p moire-ui-generated

# Run clippy on the workspace (both dev and release profile)
clippy:
    cargo clippy --locked --workspace --all-targets --no-deps --profile dev -- -D warnings --cap-lints warn
    cargo clippy --locked --workspace --all-targets --no-deps --profile release -- -D warnings --cap-lints warn

# Fix lint warnings
fix:
    cargo fix --locked --workspace --all-targets --all-features
    cargo clippy --locked --workspace --no-deps --all-targets --all-features --fix

# Run tests
test:
    RUST_BACKTRACE=1 cargo test --locked --workspace -- --nocapture
