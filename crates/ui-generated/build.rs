fn main() {
    #[cfg(all(
        not(any(windows, target_vendor = "apple")),
        not(any(feature = "backend-x11", feature = "backend-wayland"))
    ))]
    compile_error!(
        "At least one of \"backend-x11\" or \"backend-wayland\" features must be enabled (both may be enabled)."
    );

    slint_build::compile("ui/main_window.slint").unwrap();
}
