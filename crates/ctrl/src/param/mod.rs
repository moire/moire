//! Control parameters
//!
//! Live control of parameters for connecting hardware inputs (buttons, pots, faders)
//! and outputs (LEDs, visuals, haptic feedback) to the real-time kernel.

use std::borrow::Cow;

use enum_as_inner::EnumAsInner;

pub mod atomic;
pub use self::atomic::AtomicValue;

pub mod register;
pub use self::register::{Id, Param, RegisterError, Registry};

/// Direction
///
/// Defines the direction of communication of parameter values, i.e.
/// read/load or write/store. The variant names reflect the view of
/// the _provider_.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Direction {
    /// Input
    ///
    /// Read-only for the provider, who will read out the current
    /// value periodically or when triggered by an event. Providers
    /// should never write an input.
    ///
    /// Consumers should primarily write those values, although
    /// reading back out the current values is possible. Multiple
    /// consumers of an input need to coordinate themselves,
    /// otherwise values are overwritten in an uncontrolled manner.
    Input,

    /// Output
    ///
    /// Write-only for the provider, who is allowed to update and
    /// overwrite the current value at any time. Providers should
    /// never read an output.
    ///
    /// Consumers should only read output parameters. Writing them
    /// is pointless and might confuse other consumers.
    Output,
}

/// Parameter type
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum ValueType {
    /// Boolean
    Bool,
    /// 32-bit signed integer
    I32,
    /// 32-bit unsigned integer
    U32,
    /// 32-bit single-precision floating-point number
    F32,
}

/// Parameter value
///
/// Restricted to 32-bit types for portability. All values
/// need to be stored atomically.
#[derive(Debug, Clone, Copy, PartialEq, EnumAsInner, derive_more::From)]
pub enum Value {
    /// [`ValueType::Bool`] value
    Bool(bool),
    /// [`ValueType::I32`] value
    I32(i32),
    /// [`ValueType::U32`] value
    U32(u32),
    /// [`ValueType::F32`] value
    F32(f32),
}

impl Value {
    #[must_use]
    pub const fn r#type(&self) -> ValueType {
        match self {
            Self::Bool(_) => ValueType::Bool,
            Self::I32(_) => ValueType::I32,
            Self::U32(_) => ValueType::U32,
            Self::F32(_) => ValueType::F32,
        }
    }
}

/// Human-readable name
pub type Name<'a> = Cow<'a, str>;

/// Human-readable unit label
///
/// TODO: Split into `short` (mandatory, abbreviated) and `long` (optional, e.g. tooltip) parts?
pub type Unit<'a> = Cow<'a, str>;

#[derive(Debug, Clone, PartialEq)]
pub struct Descriptor<'a> {
    /// Display name.
    ///
    /// Mandatory, but could be left empty if no innate name is available.
    pub name: Name<'a>,

    /// Display unit.
    ///
    /// Unit of the value.
    pub unit: Option<Unit<'a>>,

    /// The direction.
    pub direction: Direction,

    /// Value metadata.
    pub value: ValueDescriptor,
}

#[derive(Debug, Clone, PartialEq)]
pub struct ValueDescriptor {
    /// Range restrictions
    pub range: ValueRangeDescriptor,

    /// Default value for initialization and reset.
    ///
    /// The default value implicitly determines the value type.
    pub default: Value,
}

impl ValueDescriptor {
    #[must_use]
    pub const fn r#type(&self) -> ValueType {
        self.default.r#type()
    }
}

#[derive(Debug, Clone, Default, PartialEq)]
pub struct ValueRangeDescriptor {
    /// Minimum value (inclusive)
    pub min: Option<Value>,

    /// Maximum value (inclusive)
    pub max: Option<Value>,
}

impl ValueRangeDescriptor {
    #[must_use]
    pub const fn unbounded() -> Self {
        Self {
            min: None,
            max: None,
        }
    }
}

pub type Address<'a> = Cow<'a, str>;
