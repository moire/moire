//! Unidirectional, circular, wait-free FIFO queue.
//!
//! Handy for receiving and processing commands in a realtime context.
//! The commands could be channelled through the realtime context without
//! deallocating any memory there. Instead they are returned back to the
//! producer that either recycles or drops them.

use std::ops::{Deref, DerefMut};

use llq::{Node, Queue};

/// Create a new producer/consumer circular queue.
#[must_use]
pub fn new_producer_consumer<T>() -> (Producer<T>, Consumer<T>) {
    let (producer_tx, consumer_rx) = Queue::new().split();
    let (consumer_tx, producer_rx) = Queue::new().split();
    let producer = Producer {
        tx: producer_tx,
        rx: producer_rx,
    };
    let consumer = Consumer {
        rx: consumer_rx,
        tx: consumer_tx,
    };
    (producer, consumer)
}

/// Non-realtime producer
#[allow(missing_debug_implementations)]
pub struct Producer<T> {
    tx: llq::Producer<T>,
    rx: llq::Consumer<T>,
}

impl<T> Producer<T> {
    /// Sends a new item.
    ///
    /// Might allocate a new node if no returned, unused node could be recycled.
    ///
    /// Returns the replaced item from the recycled node.
    pub fn push(&mut self, item: T) -> Option<T> {
        let (node, replaced) = if let Some(mut node) = self.pop_node() {
            // Recycle an existing node
            let replaced = std::mem::replace(&mut *node, item);
            (node, Some(replaced))
        } else {
            // Allocate a new node
            let node = Node::new(item);
            (node, None)
        };
        self.tx.push(node);
        replaced
    }

    fn pop_node(&mut self) -> Option<Node<T>> {
        self.rx.pop()
    }

    /// Free unused capacity by dropping unused nodes.
    ///
    /// Drain the [`Consumer`] first to free all nodes.
    pub fn drain(&mut self) {
        while self.pop_node().is_some() {}
    }
}

/// Consumable item
///
/// Should be handed back to the [`Consumer`] for recycling,
/// i.e. to keep it circling and avoid (de-)allocations.
#[allow(missing_debug_implementations)]
pub struct ConsumableItem<T>(llq::Node<T>);

impl<T> AsRef<T> for ConsumableItem<T> {
    fn as_ref(&self) -> &T {
        &self.0
    }
}

impl<T> AsMut<T> for ConsumableItem<T> {
    fn as_mut(&mut self) -> &mut T {
        &mut self.0
    }
}

impl<T> Deref for ConsumableItem<T> {
    type Target = T;

    fn deref(&self) -> &T {
        self.as_ref()
    }
}

impl<T> DerefMut for ConsumableItem<T> {
    fn deref_mut(&mut self) -> &mut T {
        self.as_mut()
    }
}

/// Realtime consumer
#[allow(missing_debug_implementations)]
pub struct Consumer<T> {
    rx: llq::Consumer<T>,
    tx: llq::Producer<T>,
}

impl<T> Consumer<T> {
    /// Fetch the next item from the queue to handle it.
    pub fn pop(&mut self) -> Option<ConsumableItem<T>> {
        self.rx.pop().map(ConsumableItem)
    }

    /// Push an item back into the queue for recycling.
    pub fn push_back(&mut self, item: ConsumableItem<T>) {
        self.tx.push(item.0);
    }

    /// Consume all pending items be pushing them back.
    pub fn drain(&mut self) {
        while let Some(item) = self.pop() {
            self.push_back(item);
        }
    }
}
