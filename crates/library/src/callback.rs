//! Synchronous callbacks
//!
//! Could be attached to UI controls.

use std::sync::{
    Arc,
    atomic::{AtomicBool, Ordering},
};

use aoide::desktop_app::{ActionEffect, fs::choose_directory};
use unnest::some_or_return;

use crate::{
    Library, TRACK_REPO_SEARCH_PREFETCH_LIMIT, search::parse_track_search_filter_from_input,
};

// Simplified, local state management. A more sophisticated solution
// that enables/disables the corresponding buttons accordingly would
// require an application-wide global state management.
static ON_CHOOSE_MUSIC_DIR_GUARD: AtomicBool = AtomicBool::new(false);

#[allow(clippy::missing_panics_doc)] // Infallible
pub fn on_music_dir_choose(library: &Library, rt: tokio::runtime::Handle) -> impl FnMut() + use<> {
    let state = Arc::clone(library.state().settings());
    move || {
        if ON_CHOOSE_MUSIC_DIR_GUARD
            .compare_exchange(false, true, Ordering::Acquire, Ordering::Relaxed)
            .unwrap_or(true)
        {
            // Prevent opening multiple file dialogs at the same time
            log::info!("Already choosing a music directory");
            return;
        }
        log::info!("Choosing music directory...");
        let state = Arc::clone(&state);
        rt.spawn(async move {
            let old_music_dir = state.read().music_dir.clone();
            let new_music_dir = choose_directory(old_music_dir.as_deref()).await;
            if let Some(music_dir) = &new_music_dir {
                log::info!("Updating music directory: {}", music_dir.display());
                match state.update_music_dir(Some(music_dir)) {
                    ActionEffect::Changed | ActionEffect::MaybeChanged => {
                        log::info!("Music directory has been updated");
                    }
                    ActionEffect::Unchanged => {
                        log::debug!("Updating music directory had no effect");
                    }
                }
            }
            ON_CHOOSE_MUSIC_DIR_GUARD
                .compare_exchange(true, false, Ordering::Release, Ordering::Relaxed)
                .expect("infallible");
        });
    }
}

pub fn on_music_dir_reset(library: &Library) -> impl FnMut() + use<> {
    let state = Arc::clone(library.state().settings());
    move || {
        log::info!("Resetting music directory");
        match state.update_music_dir(None) {
            ActionEffect::Changed | ActionEffect::MaybeChanged => {
                log::info!("Music directory has been reset");
            }
            ActionEffect::Unchanged => {
                log::debug!("Resetting music directory had no effect");
            }
        }
    }
}

pub fn on_collection_synchronize_music_dir(
    library: &Library,
    rt: tokio::runtime::Handle,
) -> impl FnMut() + use<> {
    let env = Arc::downgrade(library.env());
    let state = Arc::downgrade(library.state().collection());
    move || {
        let env = some_or_return!(env.upgrade());
        let state = some_or_return!(state.upgrade());
        let (_effect, result) = state.spawn_synchronizing_vfs_task(&rt, &env);
        match result {
            Ok(_task) => {
                log::info!("Synchronizing collection with file system...");
                // TODO: Keep the task handle for reporting progress,
                // fetching the outcome, and aborting it.
            }
            Err(err) => {
                log::warn!("Failed to synchronize collection with file system: {err:#}");
            }
        }
    }
}

pub fn on_track_search_submit_input(library: &Library) -> impl FnMut(String) -> String + use<> {
    let state = Arc::clone(library.state.track_search());
    move |input| {
        log::debug!("Received search input: {input}");
        let input = input.trim().to_owned();
        let filter = parse_track_search_filter_from_input(&input);
        let resolve_url_from_content_path = state
            .read()
            .default_params()
            .resolve_url_from_content_path
            .clone();
        let mut params = aoide::api::track::search::Params {
            filter,
            ordering: vec![], // TODO
            resolve_url_from_content_path,
        };
        // Argument is consumed when updating succeeds
        match state.update_params(&mut params) {
            ActionEffect::Changed | ActionEffect::MaybeChanged => {
                log::debug!("Track search params updated");
            }
            ActionEffect::Unchanged => {
                log::debug!("Track search params {params:?} not updated");
            }
        }
        input
    }
}

pub fn on_track_search_fetch_more(
    library: &Library,
    rt: tokio::runtime::Handle,
) -> impl FnMut() + use<> {
    let env = Arc::downgrade(library.env());
    let state = Arc::downgrade(library.state.track_search());
    move || {
        let env = some_or_return!(env.upgrade());
        let state = some_or_return!(state.upgrade());
        match state.spawn_fetching_more_task(&rt, &env, Some(TRACK_REPO_SEARCH_PREFETCH_LIMIT)) {
            ActionEffect::Changed | ActionEffect::MaybeChanged => {
                log::info!("Fetching more search results...");
            }
            ActionEffect::Unchanged => {
                log::debug!("Cannot fetch more search results");
            }
        }
    }
}
