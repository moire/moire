#![warn(rust_2018_idioms)]
#![warn(rust_2021_compatibility)]
#![warn(missing_debug_implementations)]
#![warn(unreachable_pub)]
#![warn(unsafe_code)]
#![warn(rustdoc::broken_intra_doc_links)]
#![warn(clippy::pedantic)]
#![warn(clippy::clone_on_ref_ptr)]
#![allow(clippy::missing_errors_doc)]

use std::num::NonZeroUsize;
use std::{path::PathBuf, sync::Arc};

use aoide::api::{Pagination, media::source::ResolveUrlFromContentPath};
use aoide::desktop_app::collection::RestoreEntityStrategy;
use aoide::desktop_app::{Environment, collection, settings, track};
use aoide::{CollectionUid, TrackEntity, TrackUid};

pub mod callback;
pub mod search;

const CREATE_NEW_COLLECTION_ENTITY_IF_NOT_FOUND: RestoreEntityStrategy =
    RestoreEntityStrategy::LoadOrCreateNew;

const PERMIT_NESTED_MUSIC_DIRS: collection::NestedMusicDirectoriesStrategy =
    collection::NestedMusicDirectoriesStrategy::Permit;

// We always need the URL in addition to the virtual file path
const RESOLVE_TRACK_URL_FROM_CONTENT_PATH: Option<ResolveUrlFromContentPath> =
    Some(ResolveUrlFromContentPath::CanonicalRootUrl);

fn default_track_search_params() -> aoide::api::track::search::Params {
    aoide::api::track::search::Params {
        resolve_url_from_content_path: RESOLVE_TRACK_URL_FROM_CONTENT_PATH.clone(),
        ..Default::default()
    }
}

const TRACK_REPO_SEARCH_PREFETCH_LIMIT_USIZE: usize = 100;
const TRACK_REPO_SEARCH_PREFETCH_LIMIT: NonZeroUsize =
    NonZeroUsize::MIN.saturating_add(TRACK_REPO_SEARCH_PREFETCH_LIMIT_USIZE - 1);

/// Stateful library frontend.
///
/// Manages the application state that should not depend on any
/// particular UI technology.
#[derive(Clone)]
#[allow(missing_debug_implementations)]
pub struct LibraryState {
    settings: Arc<settings::SharedState>,
    collection: Arc<collection::SharedState>,
    track_search: Arc<track::repo_search::SharedState>,
}

impl LibraryState {
    #[must_use]
    pub fn new(initial_settings: settings::State) -> Self {
        let initial_track_search = track::repo_search::State::new(default_track_search_params());
        Self {
            settings: Arc::new(settings::SharedState::new(initial_settings)),
            collection: Arc::default(),
            track_search: Arc::new(track::repo_search::SharedState::new(initial_track_search)),
        }
    }

    /// Observable settings state.
    #[must_use]
    pub fn settings(&self) -> &Arc<settings::SharedState> {
        &self.settings
    }

    /// Observable collection state.
    #[must_use]
    pub fn collection(&self) -> &Arc<collection::SharedState> {
        &self.collection
    }

    /// Observable track (repo) search state.
    #[must_use]
    pub fn track_search(&self) -> &Arc<track::repo_search::SharedState> {
        &self.track_search
    }
}

/// Library state with a handle to the runtime environment
#[derive(Clone)]
#[allow(missing_debug_implementations)]
pub struct Library {
    env: Arc<Environment>,
    state: LibraryState,
}

impl Library {
    #[must_use]
    pub fn new(env: Environment, initial_settings: settings::State) -> Self {
        Self {
            env: Arc::new(env),
            state: LibraryState::new(initial_settings),
        }
    }

    #[must_use]
    pub fn env(&self) -> &Arc<Environment> {
        &self.env
    }

    #[must_use]
    pub fn state(&self) -> &LibraryState {
        &self.state
    }

    /// Spawn reactive background tasks
    pub fn spawn_background_tasks(&self, rt: &tokio::runtime::Handle, settings_dir: PathBuf) {
        rt.spawn(settings::tasklet::on_state_changed_save_to_file(
            &self.state.settings.observe(),
            settings_dir,
            |err| {
                log::error!("Failed to save settings to file: {err}");
            },
        ));
        rt.spawn(collection::tasklet::on_settings_state_changed(
            rt.clone(),
            Arc::downgrade(&self.env),
            Arc::downgrade(&self.state.collection),
            &self.state.settings,
            CREATE_NEW_COLLECTION_ENTITY_IF_NOT_FOUND,
            PERMIT_NESTED_MUSIC_DIRS,
        ));
        rt.spawn(track::repo_search::tasklet::on_collection_state_changed(
            &self.state.collection,
            Arc::downgrade(&self.state.track_search),
        ));
        rt.spawn(track::repo_search::tasklet::on_should_prefetch(
            rt.clone(),
            Arc::downgrade(&self.env),
            &self.state.track_search,
            Some(TRACK_REPO_SEARCH_PREFETCH_LIMIT),
        ));
    }
}

/// Load multiple track entities from the given collection.
pub async fn load_tracks_from_collection_by_uid(
    env: &Environment,
    collection_uid: CollectionUid,
    track_uids: Vec<TrackUid>,
) -> anyhow::Result<Vec<TrackEntity>> {
    let params = aoide::api::track::search::Params {
        filter: Some(aoide::api::track::search::Filter::AnyTrackUid(track_uids)),
        ..default_track_search_params()
    };
    let pagination = Pagination::default();
    let entities = aoide::backend_embedded::track::search(
        env.db_gatekeeper(),
        collection_uid,
        params,
        pagination,
    )
    .await?;
    Ok(entities)
}

/// Load multiple track entities from the current collection.
pub async fn load_tracks_from_current_collection_by_uid(
    env: &Environment,
    collection_state: &collection::SharedState,
    track_uids: Vec<TrackUid>,
) -> anyhow::Result<Vec<TrackEntity>> {
    let collection_uid = if let Some(collection_uid) = collection_state.read().entity_uid() {
        collection_uid.clone()
    } else {
        anyhow::bail!("No current collection");
    };
    load_tracks_from_collection_by_uid(env, collection_uid, track_uids).await
}
