use audio_viz::FilteredWaveformBin;

pub mod analyzer;

// TODO: Encapsulate in a dedicated (new)type
pub type FilteredWaveform = Vec<FilteredWaveformBin>;
