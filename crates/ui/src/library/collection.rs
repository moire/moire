use std::{future::Future, sync::Arc};

use aoide::desktop_app::{
    ActionEffect,
    collection::{
        self, State, SynchronizingVfsContext, SynchronizingVfsFinishedState, SynchronizingVfsState,
    },
};
use discro::tasklet::OnChanged;
use slint::Weak;

use crate::{ui, util::u64_to_i32_saturating};

#[must_use]
fn update_properties_in_event_loop(
    ui: &Weak<ui::MainWindow>,
    state: &collection::State,
) -> OnChanged {
    log::debug!("Updating collection properties: {state:?}");
    let is_idle = !state.is_pending();
    let is_ready;
    let entity_uid;
    let tracks_count;
    let playlists_count;
    match state {
        collection::State::Ready { entity, summary } => {
            is_ready = true;
            entity_uid = entity.hdr.uid.to_string();
            tracks_count = u64_to_i32_saturating(summary.tracks.total_count);
            playlists_count = u64_to_i32_saturating(summary.playlists.total_count);
        }
        _ => {
            is_ready = false;
            entity_uid = state
                .entity_uid()
                .map(ToString::to_string)
                .unwrap_or_default();
            tracks_count = 0;
            playlists_count = 0;
        }
    };
    ui.upgrade_in_event_loop({
        move |ui| {
            ui.set_lib_collection_is_idle(is_idle);
            ui.set_lib_collection_is_ready(is_ready);
            ui.set_lib_collection_uid(entity_uid.into());
            ui.set_lib_collection_tracks_count(tracks_count);
            ui.set_lib_collection_playlists_count(playlists_count);
        }
    })
    .map(|()| OnChanged::Continue)
    .unwrap_or(OnChanged::Abort)
}

pub fn on_state_changed_property_updater(
    ui: Weak<ui::MainWindow>,
    rt: tokio::runtime::Handle,
    library: &crate::Library,
) -> impl Future<Output = ()> + Send + 'static + use<> {
    let env = Arc::downgrade(library.env());
    let shared_state = library.state().collection();
    let mut subscriber = shared_state.subscribe_changed();
    let shared_state = Arc::downgrade(shared_state);

    async move {
        log::debug!("Starting on_state_changed_property_updater");
        loop {
            log::debug!("Suspending on_state_changed_property_updater");
            if subscriber.changed().await.is_err() {
                log::debug!("Aborting on_state_changed_property_updater");
                break;
            }
            log::debug!("Resuming on_state_changed_property_updater");

            {
                let state = &*subscriber.read_ack();

                // Update properties.
                match update_properties_in_event_loop(&ui, state) {
                    OnChanged::Continue => (),
                    OnChanged::Abort => break,
                }

                // Leave the Synchronizing state when completed.
                match state {
                    State::SynchronizingVfs {
                        context: SynchronizingVfsContext { entity },
                        state:
                            SynchronizingVfsState::Finished(SynchronizingVfsFinishedState::Failed {
                                error,
                            }),
                    } => {
                        log::warn!(
                            "Synchronizing collection {collection_uid} with file system failed: {error}",
                            collection_uid = entity.hdr.uid
                        );
                    }
                    State::SynchronizingVfs {
                        context: SynchronizingVfsContext { entity },
                        state:
                            SynchronizingVfsState::Finished(SynchronizingVfsFinishedState::Succeeded {
                                ..
                            }),
                    } => {
                        log::info!(
                            "Synchronizing collection {collection_uid} with file system succeeded",
                            collection_uid = entity.hdr.uid
                        );
                    }
                    State::SynchronizingVfs {
                        context: SynchronizingVfsContext { entity },
                        state:
                            SynchronizingVfsState::Finished(SynchronizingVfsFinishedState::Aborted),
                    } => {
                        log::info!(
                            "Synchronizing collection {collection_uid} with file system aborted",
                            collection_uid = entity.hdr.uid
                        );
                    }
                    _ => {
                        continue;
                    }
                }

                // Implicitly release the exclusive write-lock on the shared state
                // when leaving this scope.
            }

            // Refresh from database after synchronizing completed.
            let Some(env) = env.upgrade() else {
                break;
            };
            let Some(shared_state) = shared_state.upgrade() else {
                break;
            };
            let (effect, _abort_handle) = shared_state.spawn_loading_from_database_task(&rt, &env);
            match effect {
                ActionEffect::Changed | ActionEffect::MaybeChanged => (),
                ActionEffect::Unchanged => {
                    log::warn!(
                        "Cannot refresh collection from database after synchronized with file system"
                    );
                    continue;
                }
            };
        }
    }
}
