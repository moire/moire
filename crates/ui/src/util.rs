//! Slint UI utilities

#[must_use]
pub fn u64_to_i32_saturating(value: u64) -> i32 {
    value.min(i32::MAX as u64) as i32
}
