use std::ops::{Div, Mul};

use crate::{FrameSize, Frames, IFrames, ISampleRate, SampleRate, Seconds};

impl Mul<SampleRate> for Seconds {
    type Output = Frames;

    fn mul(self, sample_rate: SampleRate) -> Self::Output {
        debug_assert!(sample_rate.hz() >= 0.0);
        Frames::new(*self * sample_rate.hz())
    }
}

impl Mul<f64> for Seconds {
    type Output = Seconds;

    fn mul(self, rhs: f64) -> Self::Output {
        Self::new(*self * rhs)
    }
}

impl Div<f64> for Seconds {
    type Output = Seconds;

    fn div(self, rhs: f64) -> Self::Output {
        Self::new(*self / rhs)
    }
}

impl Mul<f64> for SampleRate {
    type Output = SampleRate;

    fn mul(self, rhs: f64) -> Self::Output {
        debug_assert!(rhs >= 0.0);
        Self::from_hz(self.hz() * rhs)
    }
}

impl Div<f64> for SampleRate {
    type Output = SampleRate;

    fn div(self, rhs: f64) -> Self::Output {
        debug_assert!(rhs > 0.0);
        Self::from_hz(self.hz() / rhs)
    }
}

impl Div for SampleRate {
    type Output = f64;

    fn div(self, rhs: Self) -> Self::Output {
        debug_assert!(rhs.hz() > 0.0);
        self.hz() / rhs.hz()
    }
}

impl Mul<ISampleRate> for Seconds {
    type Output = Frames;

    fn mul(self, sample_rate: ISampleRate) -> Self::Output {
        self * SampleRate::from(sample_rate)
    }
}

impl Mul<f64> for Frames {
    type Output = Frames;

    fn mul(self, rhs: f64) -> Self::Output {
        Self::new(*self * rhs)
    }
}

impl Div<f64> for Frames {
    type Output = Frames;

    fn div(self, rhs: f64) -> Self::Output {
        Self::new(*self / rhs)
    }
}

impl Div<SampleRate> for Frames {
    type Output = Seconds;

    fn div(self, sample_rate: SampleRate) -> Self::Output {
        debug_assert!(sample_rate.hz() > 0.0);
        Seconds::new(*self / sample_rate.hz())
    }
}

impl Div<ISampleRate> for Frames {
    type Output = Seconds;

    fn div(self, sample_rate: ISampleRate) -> Self::Output {
        self / SampleRate::from(sample_rate)
    }
}

impl Div<SampleRate> for IFrames {
    type Output = Seconds;

    fn div(self, sample_rate: SampleRate) -> Self::Output {
        debug_assert!(sample_rate.hz() > 0.0);
        Seconds::new(*self as f64 / sample_rate.hz())
    }
}

impl Div<SampleRate> for FrameSize {
    type Output = Seconds;

    fn div(self, sample_rate: SampleRate) -> Self::Output {
        debug_assert!(sample_rate.hz() > 0.0);
        Seconds::new(*self as f64 / sample_rate.hz())
    }
}
