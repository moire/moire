#![warn(rust_2018_idioms)]
#![warn(rust_2021_compatibility)]
#![warn(missing_debug_implementations)]
#![warn(unreachable_pub)]
#![warn(unsafe_code)]
#![warn(rustdoc::broken_intra_doc_links)]
#![warn(clippy::pedantic)]
#![warn(clippy::clone_on_ref_ptr)]
#![allow(clippy::missing_errors_doc)]
#![allow(clippy::module_name_repetitions)]

use std::{borrow::Cow, collections::HashSet, time::Duration};

use hidapi::{DeviceInfo, HidApi, HidDevice, HidError};
use thiserror::Error;

pub mod ni_traktor_s4mk3;

pub mod report;

pub mod thread;
pub use thread::Thread;

#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    HidApi(#[from] HidError),

    #[error("device not connected")]
    DeviceNotConnected,

    #[error("device not supported")]
    DeviceNotSupported,

    #[error(transparent)]
    Unexpected(#[from] anyhow::Error),
}

// from HID specification
// https://www.usb.org/document-library/hid-usage-tables-13
#[derive(Copy, Clone, Debug, PartialEq, Eq, Hash)]
pub enum UsagePage {
    Undefined,
    GenericDesktop,
    SimulationControls,
    VRControls,
    SportControls,
    GameControls,
    GenericDeviceControls,
    Keyboard,
    LED,
    Button,
    Ordinal,
    Telephony,
    Consumer,
    Digitizer,
    Haptics,
    PhysicalInput,
    Unicode,
    EyeAndHeadTracker,
    AuxiliaryDisplay,
    Sensors,
    MedicalInstrument,
    BrailleDisplay,
    Light,
    Monitor,
    MonitorEnumerated,
    VESAVirtualControls,
    Power,
    BatterySystem,
    BarcodeScanner,
    Scale,
    MagneticStripeReader,
    CameraControl,
    Arcade,
    GamingDevice,
    FIDO,
    Reserved(u16),
    VendorDefined(u16),
}

impl From<u16> for UsagePage {
    fn from(number: u16) -> UsagePage {
        #[allow(clippy::match_same_arms)]
        match number {
            0x00 => UsagePage::Undefined,
            0x01 => UsagePage::GenericDesktop,
            0x02 => UsagePage::SimulationControls,
            0x03 => UsagePage::VRControls,
            0x04 => UsagePage::SportControls,
            0x05 => UsagePage::GameControls,
            0x06 => UsagePage::GenericDeviceControls,
            0x07 => UsagePage::Keyboard,
            0x08 => UsagePage::LED,
            0x09 => UsagePage::Button,
            0x0a => UsagePage::Ordinal,
            0x0b => UsagePage::Telephony,
            0x0c => UsagePage::Consumer,
            0x0d => UsagePage::Digitizer,
            0x0e => UsagePage::Haptics,
            0x0f => UsagePage::PhysicalInput,
            0x10 => UsagePage::Unicode,
            0x11 => UsagePage::Reserved(number),
            0x12 => UsagePage::EyeAndHeadTracker,
            0x13 => UsagePage::Reserved(number),
            0x14 => UsagePage::AuxiliaryDisplay,
            0x15..=0x1f => UsagePage::Reserved(number),
            0x20 => UsagePage::Sensors,
            0x21..=0x3f => UsagePage::Reserved(number),
            0x40 => UsagePage::MedicalInstrument,
            0x41 => UsagePage::BrailleDisplay,
            0x42..=0x58 => UsagePage::Reserved(number),
            0x59 => UsagePage::Light,
            0x5a..=0x7f => UsagePage::Reserved(number),
            0x80 => UsagePage::Monitor,
            0x81 => UsagePage::MonitorEnumerated,
            0x82 => UsagePage::VESAVirtualControls,
            0x83 => UsagePage::Reserved(number),
            0x84 => UsagePage::Power,
            0x85 => UsagePage::BatterySystem,
            0x86..=0x8b => UsagePage::Reserved(number),
            0x8c => UsagePage::BarcodeScanner,
            0x8d => UsagePage::Scale,
            0x8e => UsagePage::MagneticStripeReader,
            0x8f => UsagePage::Reserved(number),
            0x90 => UsagePage::CameraControl,
            0x91 => UsagePage::Arcade,
            0x92 => UsagePage::GamingDevice,
            0x93..=0xf1cf => UsagePage::Reserved(number),
            0xf1d0 => UsagePage::FIDO,
            0xf1d1..=0xfeff => UsagePage::Reserved(number),
            0xff00..=0xffff => UsagePage::VendorDefined(number),
        }
    }
}

pub type Result<T> = std::result::Result<T, Error>;

#[allow(missing_debug_implementations)]
pub struct Api {
    hid_api: HidApi,
}

impl Api {
    pub fn new() -> Result<Self> {
        let hid_api = HidApi::new_without_enumerate()?;
        Ok(Self { hid_api })
    }

    pub fn query_devices(&mut self) -> Result<impl Iterator<Item = &DeviceInfo>> {
        self.hid_api.refresh_devices()?;
        Ok(self.hid_api.device_list())
    }

    pub fn query_devices_dedup(&mut self) -> Result<Vec<Device>> {
        let mut visited_paths = HashSet::new();
        Ok(self
            .query_devices()?
            .filter(|info| visited_paths.insert(info.path()))
            .map(|info| Device::new(info.clone()))
            .collect())
    }

    pub fn query_device_by_id(&mut self, id: &DeviceId<'_>) -> Result<Option<Device>> {
        Ok(self.query_devices()?.find_map(|info| {
            let found_id = DeviceId::try_from(info).ok();
            if Some(id) == found_id.as_ref() {
                return Some(Device::new(info.clone()));
            }
            None
        }))
    }

    pub fn connect_device(&self, info: DeviceInfo) -> Result<Device> {
        let mut device = Device::new(info);
        device.connect(self)?;
        Ok(device)
    }
}

const INF_TIMEOUT_MILLIS: i32 = -1;
const MAX_TIMEOUT_MILLIS: i32 = i32::MAX;

#[allow(clippy::cast_possible_truncation)]
fn timeout_millis(timeout: Option<Duration>) -> i32 {
    // Verify that the timeout is specified in full milliseconds
    // to prevent losing precision unintentionally.
    debug_assert_eq!(0, timeout.unwrap_or_default().subsec_nanos() % 1_000_000);
    timeout
            .as_ref()
            .map(Duration::as_millis)
            // Saturating conversion from u128 to i32
            .map_or(INF_TIMEOUT_MILLIS, |millis| millis.min(MAX_TIMEOUT_MILLIS as _) as _)
}

#[allow(missing_debug_implementations)]
pub struct Device {
    info: DeviceInfo,

    // The Option tracks whether the device is connected (= has been opened).
    hid_dev: Option<HidDevice>,
}

/// Permanent, connection-independent device identifier.
///
/// Could be used for referencing devices persistently, e.g. in configurations.
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
#[cfg_attr(feature = "serde", derive(serde::Serialize, serde::Deserialize))]
pub struct DeviceId<'a> {
    /// Vendor id
    pub vid: u16,

    /// Product id
    pub pid: u16,

    /// Non-empty serial number
    pub sn: Cow<'a, str>,
}

impl DeviceId<'_> {
    #[must_use]
    pub fn into_owned(self) -> DeviceId<'static> {
        let Self { vid, pid, sn } = self;
        DeviceId {
            vid,
            pid,
            sn: Cow::Owned(sn.into_owned()),
        }
    }
}

impl<'a> TryFrom<&'a DeviceInfo> for DeviceId<'a> {
    type Error = ();

    #[allow(clippy::similar_names)]
    fn try_from(from: &'a DeviceInfo) -> std::result::Result<Self, Self::Error> {
        if let Some(sn) = from.serial_number() {
            let sn = sn.trim();
            if !sn.is_empty() {
                let vid = from.vendor_id();
                let pid = from.product_id();
                let sn = Cow::Borrowed(sn);
                return Ok(Self { vid, pid, sn });
            }
        }
        Err(())
    }
}

impl Device {
    #[must_use]
    pub fn new(info: DeviceInfo) -> Self {
        Self {
            info,
            hid_dev: None,
        }
    }

    #[must_use]
    pub fn info(&self) -> &DeviceInfo {
        &self.info
    }

    #[must_use]
    pub fn is_connected(&self) -> bool {
        self.hid_dev.is_some()
    }

    pub fn connect(&mut self, api: &Api) -> Result<()> {
        if self.is_connected() {
            return Ok(());
        }
        let hid_dev = api.hid_api.open_path(self.info.path())?;
        // Blocking is controlled explicitly by a timeout with each read request.
        // The following function is only called as a safeguard to ensure a consistent
        // initial state.
        hid_dev.set_blocking_mode(true)?;
        self.hid_dev = Some(hid_dev);
        debug_assert!(self.is_connected());
        Ok(())
    }

    pub fn disconnect(&mut self) {
        // The optional `HidDevice` will implicitly be dropped and closed by the assignment.
        self.hid_dev = None;
        debug_assert!(!self.is_connected());
    }

    fn hid_dev(&self) -> Result<&HidDevice> {
        self.hid_dev.as_ref().ok_or(Error::DeviceNotConnected)
    }

    pub fn get_feature_report(&self, buffer: &mut [u8]) -> Result<usize> {
        let hid_dev = self.hid_dev()?;
        Ok(hid_dev.get_feature_report(buffer)?)
    }

    pub fn send_feature_report(&self, data: &[u8]) -> Result<()> {
        let hid_dev = self.hid_dev()?;
        Ok(hid_dev.send_feature_report(data)?)
    }

    /// Blocking read into buffer with optional timeout (millisecond precision).
    pub fn read(&self, buffer: &mut [u8], timeout: Option<Duration>) -> Result<usize> {
        let hid_dev = self.hid_dev()?;
        let timeout_millis = timeout_millis(timeout);
        Ok(hid_dev.read_timeout(buffer, timeout_millis)?)
    }

    pub fn write(&self, data: &[u8]) -> Result<usize> {
        let hid_dev = self.hid_dev()?;
        Ok(hid_dev.write(data)?)
    }
}
