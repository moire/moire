# Roadmap

These are presented in a proposed order of implementation, particularly the earlier features, but may end up getting implemented in a different order.

- [x] Play a file from a path specified on the command line with filesystem access and audio processing on separate threads
- [x] Show a GUI with the elapsed playback time
- [x] Click and drag to move the tracks on the timeline.
- [ ] Add a separate playhead for the headphones output
- [ ] Add a volume slider for each track and figure out how it will relate to both the main output and headphones output
- [ ] Add separate outputs for each player before they are mixed together
- [ ] Add a crossfader
- [ ] Add a prefader gain knob
- [ ] Start a MIDI & HID controller system and map the existing controls to a controller using each protocol
- [ ] Start an effects system and use it to implement a 3 band equalizer
- [ ] Map the equalizer's knobs to HID & MIDI controllers
- [ ] Add a tempo slider to control playback rate. Simply use resampling for now without implementing keylock yet.
- [ ] Add a simple GUI for selecting files from an [Aoide](https://gitlab.com/uklotzde/aoide-rs) music library and loading them to tracks
- [ ] Add buttons for marking and seeking to hotcues and intro/outro sections
- [ ] Add a GUI for searching and sorting the music library with Aoide
- [ ] Add an audio feature analysis framework and use [ebur128](https://github.com/sdroege/ebur128) for automatic loudness normalization
- [ ] Explore options for musical key detection
- [ ] Explore options for beat detection
- [ ] Create a GUI for editing musical time annotations with variable time signatures and tempos over the course of a track
- [ ] Add support for beatjumping and map this to controller buttons and encoders
- [ ] Add support for setting, toggling, and storing loop cues with loop size controlled by an encoder
- [ ] Add support for scratching with controller jog wheels, including motorized wheels
- [ ] Create a feature for precisely aligning arbitrary points in two tracks with an interface to align hotcues or intro/outro cues
- [ ] Implement keylock to adjust the tempo while keeping pitch constant
- [ ] Implement key adjustment while keeping tempo constant
- [ ] Add postfader effects chains
- [ ] Add per-effect metaknobs and per-chain superknobs for easy use of effects (terminology borrowed from Mixxx and may change)
- [ ] Save and load effect chain presets
- [ ] Add a switch for using a live input for tracks
- [ ] Add a feature for recording a live input to a deck
- [ ] Add a feature for recording the mix
- [ ] Add a system for synchronizing the tempos of multiple decks
